# delightful activitypub development [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A curated list of resources for ActivityPub developers who create software for the Fediverse.

Emoji's for each entry indicate whether it is added to the [Fediverse Party](https://fediverse.party) website:

* :heavy_check_mark: == added to live website at [**fediverse.party**](https://fediverse.party) (Only set by [@lostinlight](https://codeberg.org/lostinlight))
* :black_nib: == newly added to this page (Please use this emoji in your PR's)

## Contents

- [Developer tools](#developer-tools)
  - [Libraries](#libraries)
  - [Plugins](#plugins)
  - [Relays](#relays)
  - [Bridges](#bridges)
  - [Utilities](#utilities)
  - [Testing](#testing)
- [Miscellaneous projects](#miscellaneous-projects)
  - [Search](#search)
  - [Social sharing](#social-sharing)
  - [Other projects](#other-projects)
- [Reference material](#reference-material)
  - [Protocol specifications](#protocol-specifications)
  - [API documentation](#api-documentation)
- [Tutorials](#tutorials)
  - [Getting started](#getting-started)
  - [ActivityPub server-to-server (S2S)](#activitypub-server-to-server-s2s)
  - [ActivityPub client-to-server (C2S)](#activitypub-server-to-server-s2s)
  - [WebFinger](#webfinger)
  - [Security](#security)
- [Research & Development](#research-development)
  - [Datashards](#datashards)
  - [Object capabilities](#object-capabilities)
  - [Federated authentication](#federated-authentication)
  - [Content addressing](#content-addressing)
  - [Peer-to-peer networking](peer-to-peer-networking)
- [Maintainers](#maintainers)
- [Contributors](#contributors)
- [License](#license)

## Developer tools

### Frameworks

* :heavy_check_mark: [**Bonfire**](https://bonfirenetworks.org): An extensible framework with a big focus on customisation and flexibility. Can be used to build new federated apps while focusing on a specific use case rather than reimplementing lots of boilerplate   `AGPL-3.0, Elixir`

#### Libraries

* :heavy_check_mark: [**ActivityPhp**](https://github.com/landrok/activitypub) ([site](https://landrok.github.io/activitypub), [Fedi account](https://cybre.space/@landrok)): A PHP implementation of ActivityPub protocol based upon the ActivityStreams 2.0 data format. `MIT, PHP`

* :heavy_check_mark: [**ActivityPods**](https://github.com/assemblee-virtuelle/activitypods): Adding intelligence to Solid PODs with ActivityPub (based on [SemApps](https://semapps.org). `Apache-2.0, Javascript`

* :heavy_check_mark: [**ActivityPub**]https://github.com/bonfire-networks/activity_pub): Generic library to federate an app with ActivityPub (uses a queue for incoming/outgoing activities and adapter modules for tight integration). `AGPL-3.0, Elixir`

* :heavy_check_mark: [**ActivityPub Express**](https://github.com/immers-space/activitypub-express): Modular ActivityPub implementation as Express.js middleware to easily add decentralization and federation to Node apps `-, Javascript`

* :heavy_check_mark: [**ActivityPub-PHP**](https://github.com/pterotype-project/activitypub-php) ([Fedi account](https://mastodon.technology/@jdormit)): A PHP implementation of the ActivityPub protocol (used in Pterotype plugin).  `MIT, PHP`

* :heavy_check_mark: [**activityPub4j**](https://github.com/msummers/activityPub4j): W3C ActivityPub and ActivityStreams implementation in Java using Spring Boot. `Apache-2.0, Java`

* :heavy_check_mark: [**ActivityStreams**](https://github.com/OpenSocial/activitystreams): Full ActivityStreams 1.0 and 2.0 reference implementation in Java. `Apache-2.0, Java`

* :heavy_check_mark: [**ActivityServe**](https://github.com/writeas/activityserve): A very light ActivityPub library in Go (used to power [pherephone](https://github.com/writeas/pherephone)) `MIT, Go`

* :heavy_check_mark: [**ActivityStreams-2**](https://github.com/gobengo/activitystreams2): ActivityStreams 2.0 library for Node.js and TypeScript. `Apache-2.0, Typescript`

* :heavy_check_mark: [**astreams**](https://github.com/MatejLach/astreams) ([Fedi account](https://social.matej-lach.me/@MatejLach)): A hand-crafted implementation of the Activity Streams 2.0 specification in Go, especially suitable for projects implementing ActivityPub. `AGPL-3.0, Go`

* :heavy_check_mark: [**atoot**](https://github.com/popura-network/atoot): Library providing an easy way to create Mastodon API applications `MIT, Python`

* :black_nib: [**Clovis**](https://github.com/WellFactored/clovis): [ARCHIVED] Implementation of ActivityPub in Scala. (Hobby project included here for reference only.) `AGPL-3.0, Scala`

* :heavy_check_mark: [**corpus-activity-streams**](https://github.com/ryanatkn/corpus-activity-streams): Activity Streams 2.0 vocabulary data and alternative docs. `Unlicense license, Typescript`

* :black_nib: [**dda-masto-embed**](https://github.com/DomainDrivenArchitecture/dda-masto-embed) ([Fedi account](https://social.meissa-gmbh.de/@team)): Embeds mastodon timline into a html page. Uses JS, no intermediate server required. `Apache-2.0, ClojureScript` 

* :heavy_check_mark: [**Express ActivityPub**](https://github.com/dariusk/express-activitypub): A very simple reference implementation of an ActivityPub server using Express.js. `MIT, Javascript`

* :heavy_check_mark: [**Federation**](https://git.feneas.org/jaywink/federation): Library to abstract social web federation protocols like ActivityPub, Diaspora and Matrix (see [docs](https://federation.readthedocs.io)) `BSD-3-clause, Python`

* :heavy_check_mark: [**Golang ActivityPub**](https://github.com/go-ap) ([Fedi account](https://metalhead.club/@mariusor)): Libraries for using ActivityPub in the Go language. `MIT, Go`

* :heavy_check_mark: [**go-fed activity**](https://github.com/go-fed/activity) ([site](https://go-fed.org), [Fedi account](https://mastodon.technology/@cj)): Full ActivityStreams & ActivityPub implementation in Golang. Extensions can be easily added by design-time code generation from JSON-LD schema's (also supports [ForgeFed](https://forgefed.peers.community) this way, by default). `BSD-3-clause, Go`

* :heavy_check_mark: [**go-fed apcore**](https://github.com/go-fed/apcore) ([site](https://go-fed.org), [Fedi account](https://mastodon.technology/@cj)): A powerful single server ActivityPub framework for performant Fediverse applications. `AGPL-3.0, Go`

* :black_nib: [**go-mastodon**](https://github.com/mattn/go-mastodon): Mastodon client for Golang. `MIT, Go`

* :heavy_check_mark: [**http-signature**](https://github.com/mtti/http-signature): Implementation of the HTTP Signature scheme as used in ActivityPub. `Apache-2.0, Typescript`

* :heavy_check_mark: [**Little Boxes**](https://little-boxes.readthedocs.io): Tiny ActivityPub framework, both database and server agnostic `ISC, Python`

* :heavy_check_mark: [**Little Library**](https://github.com/Alamantus/little-library): A digital give-a-book, take-a-book library for ebooks `AGPL-3.0, Javascript`

* :heavy_check_mark: [**Mastodon.py**](https://github.com/halcy/Mastodon.py): Python wrapper for the Mastodon API. `MIT, Python`

* :heavy_check_mark: [**Pubstrate**](https://gitlab.com/dustyweb/pubstrate): ActivityStreams and ActivityPub library implementation for GNU Guile. Includes a full ActivityStreams library and most of an ActivityPub implementation. `GPL-3.0, Guile`

* :heavy_check_mark: [**Python ActivityPub**](https://github.com/dsblank/activitypub): A general ActivityPub library `MPL-2.0, Python`

* :black_nib: [**rdf-pub**](https://gitlab.com/linkedopenactors/rdf-pub) ([site](https://linkedopenactors.gitlab.io/rdf-pub/), [Fedi account](https://chaos.social/@naturzukunft)): An activity-pub server implementation, that is not limited to the activity-stream vocabulary, but supports RDF per se. `EUPL, Java`

#### Plugins

* :heavy_check_mark: [**ActivityPub for Drupal**](https://github.com/swentel/activitypub) ([Lead dev](https://realize.be/user/1)): ActivityPub integration for Drupal 8 (see also [lite version](https://www.drupal.org/project/activitypub/) at Drupal) `GPL-2.0, PHP`

* :heavy_check_mark: [**FediEmbedi**](https://git.feneas.org/mediaformat/fediembedi) ([Fedi account](https://social.coop/@django)): A wordpress plugin widget to display your fediverse account timeline. `GPL-3.0, PHP`

* :heavy_check_mark: [**Hugo-ActivityStreams**](https://git.jlel.se/jlelse/hugo-activitystreams): A Hugo module for use in Hugo, to generate ActivityStreams representations of posts. ([mirror](https://codeberg.org/jlelse/hugo-activitystreams)) `MIT, Go`

* :heavy_check_mark: [**Pterotype**](https://github.com/pterotype-project/pterotype): WordPress plugin. Pterotype connects your blog to the Fediverse by giving it an ActivityPub feed `MIT, PHP`

* :heavy_check_mark: [**WordPress-ActivityPub**](https://github.com/pfefferle/wordpress-activitypub): ActivityPub for Wordpress `MIT, PHP`

* :heavy_check_mark: [**WordPress-OStatus**](hhttps://github.com/pfefferle/wordpress-ostatus): An OStatus plugin for WordPress `MIT, PHP`

* :heavy_check_mark: [**XWiki Extension for ActivityPub**](https://github.com/xwiki-contrib/application-activitypub) ([site](https://extensions.xwiki.org/xwiki/bin/view/Extension/ActivityPub%20Application/), [Fedi account](https://social.weho.st/@XWiki)): An implementation of the ActivityPub protocol for XWiki (see [forum discussion](https://forum.xwiki.org/t/new-application-activitypub/6186)). `LGPL-2.1, Java`

#### Relays

* :heavy_check_mark: [**ActivityRelay**](https://git.pleroma.social/pleroma/relay): A generic LitePub relay (works with all LitePub consumers and Mastodon). `AGPL-3.0, Python`

* :heavy_check_mark: [**activity-relay**](https://github.com/yukimochi/Activity-Relay): Yet another powerful customizable ActivityPub relay server written in Go `AGPL-3.0, Go`

* :heavy_check_mark: [**Hash2Pub**](https://git.orlives.de/schmittlauch/Hash2Pub) ([Fedi account](https://toot.matereal.eu/@schmittlauch)): A fully-decentralised DHT-based relay for global hashtag federation. See [White paper](https://git.orlives.de/schmittlauch/paper_hashtag_federation), [status update](https://socialhub.activitypub.rocks/t/542/19) (Nov 2020) `AGPL-3.0, Haskell`

* :heavy_check_mark: [**Pub relay**](https://source.joinmastodon.org/mastodon/pub-relay) `AGPL-3.0, Crystal`

* :heavy_check_mark: [**Seattle relay**](https://gitlab.com/jankysolutions/social.seattle.wa.us/relay) `-, Python`

* :heavy_check_mark: [**Social relay**](https://git.feneas.org/jaywink/social-relay): Public post relay for the diaspora* federated social network protocol. `AGPL-3.0, Python`

For an overview of known relays, see [A sorted list of ActivityPub relays for Mastodon or Pleroma](https://github.com/brodi1/activitypub-relays).

#### Bridges

* :heavy_check_mark: [**BirdSiteLIVE**](https://github.com/NicolasConstant/BirdsiteLive) ([Fedi account](https://fosstodon.org/@BirdsiteLIVE)): An ethical bridge from Twitter `AGPL-3.0, C#`

* :heavy_check_mark: [**bleroma**](https://github.com/4DA/bleroma): Telegram bot for Pleroma and Mastodon. `MIT, Elixir`

* :heavy_check_mark: [**BridgyFed**](https://github.com/snarfed/bridgy-fed) ([site](https://fed.brid.gy)): Bridges the IndieWeb to federated social networks: ActivityPub, OStatus, etc. `Public Domain, Python`

* :heavy_check_mark: [**Fediverse-Action**](https://github.com/rzr/fediverse-action): Github Action that posts to Fediverse when code is changed. `ISC License, Javascript`

* :heavy_check_mark: [**feed2toot**](https://gitlab.com/chaica/feed2toot): Parses RSS feeds, identifies new posts and posts them on the Mastodon social network ([using the Mastodon API]((https://gitlab.com/chaica/feed2toot/issues/35#note_289027030))). `MIT, Python`

* :heavy_check_mark: [**feediverse**](https://github.com/edsu/feediverse): Send RSS/Atom feeds to Mastodon. `MIT, Python`

* :heavy_check_mark: [**gemifedi**](https://git.sr.ht/~boringcactus/gemifedi): A Gemini frontend to the fediverse (specifically, Mastodon and Pleroma instances). `AGPL-3.0, Rust`

* :heavy_check_mark: [**Instagram2Fedi**](https://github.com/Horhik/Instagram2Fedi): Script for crossposting from Instagram to Mastodon or Pixelfed. `GPL-3.0, Python`

* :heavy_check_mark: [**Kazarma**](https://gitlab.com/kazarma/kazarma/): A Matrix bridge to ActivityPub. `AGPL-3.0, Elixir`

* :heavy_check_mark: [**Libervia**](https://repos.goffi.org/sat/file/tip) ([site](https://salut-a-toi.org/), [Fedi account](https://mastodon.social/@Goffi)): An XMPP <=> ActivityPub gateway project doubled with XMPP Pubsub end-to-end encryption `AGPL-3.0, Python`

* :heavy_check_mark: [**ligh7hau5**](https://github.com/vulet/ligh7hau5): A Matrix to Fediverse / ActivityPub client / bridge. Also, some media proxying. `GPL-3.0, Javascript`

* :heavy_check_mark: [**mastodon-twitter-poster**](https://github.com/renatolond/mastodon-twitter-poster) ([site](https://crossposter.masto.donte.com.br/), [Fedi account](https://masto.donte.com.br/@crossposter)): Crossposter to post statuses between Mastodon and Twitter. `AGPL-3.0, Ruby`

* :heavy_check_mark: [**Moa**](https://gitlab.com/fedstoa/moa) ([site](https://moa.party/)): A Mastodon, Twitter, and Instagram Cross-poster. `MIT, Python`

* :heavy_check_mark: [**MXToot**](https://github.com/ma1uta/mxtoot) ([Fedi account](https://mastodon.social/@ma1uta)): A Matrix <--> Mastodon bot written in Java. `Apache-2.0, Java`

* :heavy_check_mark: **[Nautilus](https://github.com/aaronpk/Nautilus)**: A standalone service to deliver posts from your own website to ActivityPub followers. `Apache-2.0, PHP`

* :heavy_check_mark: **[PeerTube (on Matrix) Search](https://github.com/vranki/hemppa#peertube-search)**: Search PeerTube via Matrix, using Sepia Search API to search on all participating public PeerTube instances. You can also select any single instance. It's implemented as a module for Hemppa the bot. `GPL-3.0, Python`

* :heavy_check_mark: [**rss-bot**](https://alexschroeder.ch/cgit/rss-bot/) ([Fedi account](https://octodon.social/@kensanata)): Post updates from an RSS feed to Mastodon. `GPL-3.0, Python`

* :heavy_check_mark: [**RSS-to-ActivityPub Converter**](https://github.com/dariusk/rss-to-activitypub): Convert any RSS feed to an ActivityPub actor that can be followed by users on ActivityPub-compliant social networks like Mastodon. `MIT, Javascript`

* :black_nib: [**Simplebot-Mastodon**](https://github.com/simplebot-org/simplebot_mastodon): A Mastodon - DeltaChat bridge plugin for SimpleBot, [list of bot instances](https://simplebot-org.github.io/simplebot-instances) `MPL-2.0, Python`

* :heavy_check_mark: [**Twitter Hamachpil**](https://gitlab.com/hamachpil/twitter_hamachpil) ([Fedi account](https://emacsen.net/@emacsen)): Bot to grab tweets from Twitter and post them to respective accounts on a Mastodon instance. `Apache-2.0, Python`

* :heavy_check_mark: **[YouTube2PeerTube](https://github.com/mister-monster/YouTube2PeerTube)**: A bot that mirrors YouTube channels to PeerTube channels as videos are released in a YouTube channel. `AGPL-3.0, Python`

#### Utilities

* :black_nib: [**ActivityPubSchema**](https://github.com/redaktor/ActivityPubSchema): JSON Schema definition of the ActivityStreams and ActivityPub specifications. `MIT, Javascript`

* :heavy_check_mark: [**bridge**](https://source.joinmastodon.org/mastodon/bridge): A simple web app that helps you find your Twitter friends on the federated Mastodon network. It is also an example of how the Mastodon API can be used and the federated OAuth authorization flow. `AGPL-3.0, Ruby`

* :heavy_check_mark: [**FediHealth**](https://git.feneas.org/buttle/fedihealth): Software to help you define federation policies for your node. `AGPL-3.0, Python`

* :heavy_check_mark: [**Fediverse Stats**](https://gitlab.com/spla/fediverse) ([Fedi account](https://mastodont.cat/@fediverse)): Collects maximum number of alive fediverse's servers and then query their API to obtain their registered users. `GPL-3.0, Python`

* :heavy_check_mark: [**forget**](https://github.com/codl/forget) ([site](https://forget.codl.fr/), [Fedi account](https://chitter.xyz/@codl)): Continuous post deletion for Mastodon and Twitter (if you happen to use that). `ISC License, Python`

* :heavy_check_mark: [**mastodon-backup**](https://github.com/kensanata/mastodon-backup) ([Fedi account](https://octodon.social/@kensanata)): Archive your statuses, favorites and media using the Mastodon API. `GPL-3.0, Python`

* :heavy_check_mark: **[Mastotool](https://github.com/muesli/mastotool)**: A collection of tools to work with your Mastodon account; displays account statistics and lets you search your toots. `MIT, Go`

* :heavy_check_mark: [**OCR Bot**](https://github.com/Lynnesbian/OCRbot/) ([Fedi account](https://fedi.lynnesbian.space/@lynnesbian)): An OCR (Optical Character Recognition) bot for Mastodon (and compatible) instances `AGPL-3.0, Python`

* :heavy_check_mark: [**Pherephone**](https://github.com/writeas/pherephone): An ActivityPub server that reblogs all the statuses of certain actors. You set it up to follow a few accounts and it announces everything they post. `AGPL-3.0, Go`

* :heavy_check_mark: [**tags-pub**](https://gitlab.com/evanp/tags-pub): Provides hashtag objects on the ActivityPub network. `Apache-2.0, Javascript`

* :heavy_check_mark: [**yt2pt**](https://github.com/buoyantair/yt2pt): A simple set of scripts to quickly import your youtube channel to Peertube. `MIT, Javascript`

* :heavy_check_mark: [**Moderator Alerts**](https://gitlab.com/stemid/mastodon-moderator-alerts) ([Fedi account](https://mastodon.se/@stemid)): Mastodon moderation alerts using Pushover.net. `-, Python`

#### Testing

* :heavy_check_mark: [**activitypub-mock**](https://gitlab.com/evanp/activitypub-mock): A mock ActivityPub server to use in testing code `Apache-2.0, Javascript`

* :black_nib: [**dfk-ap**](https://glitch.com/edit/#!/dfk-ap?path=README.md%3A1%3A0) ([site](https://tinysubversions.com/notes/activitypub-tool/)): A small ActivityPub debugging server on Glitch `MIT, Javascript`

* :heavy_check_mark: [**FediDB**](https://fedidb.org/about): A suite of tools for AP devs to help make it easier to test and validate your implementation with existing implementations like Mastodon, PeerTube, Pixelfed and Pleroma `-, -`

* :heavy_check_mark: [**Test Suite**](https://github.com/go-fed/testsuite): An unofficial partially-automated ActivityPub test suite `AGPL-3.0, Go`

## Miscellaneous projects

This category is for any code project related to the Fediverse. They need not be directly development related.

#### Search

* :black_nib: [**Sepia Search**](https://framagit.org/framasoft/peertube/search-index/) ([site](https://sepiasearch.org/)): A search engine of PeerTube videos and channels Developed by Framasoft `AGPL-3.0, Typescript`

#### Social sharing

* :heavy_check_mark: [**Share Buttons**](https://git.fsfe.org/FSFE/share-buttons): Share buttons that support dynamic input of Fediverse URLs and require no Javascript. `AGPL-3,0, PHP`

* :heavy_check_mark: [**Fedishare**](https://gitlab.com/mugcake/fedishare): Firefox toolbar extension to share the current browser tab on the Fediverse `GPL-3.0, Typescript`

#### Other projects

* :black_nib: [**Mastodon Simplified Federation**](https://github.com/rugk/mastodon-simplified-federation): Simplifies following and interacting with remote users on other Mastodon instances. `ISC, Javascript`

## Reference material

#### Protocol specifications

* :heavy_check_mark: [**ForgeFed**](https://notabug.org/peers/forgefed), formerly GitPub ([site](https://forgefed.peers.community), [Fedi account](https://floss.social/@forgefed)): A set of extensions to ActivityPub for federation between code forges (i.e. git hosting sites like GitLab, Gogs, Gitea, etc. Reference implementation is [Vervis](https://dev.angeley.es/s/fr33domlover/p/vervis)). `CC0-1.0`

* :heavy_check_mark: [**LitePub**](https://github.com/litepub/litepub) ([Fedi account](https://pleroma.site/users/kaniini)): A set of extensions to AP, being developed by devs from Pleroma and Mastodon (status: for the most part litepub group folded back into SocialCG, see: [issue](https://github.com/litepub/litepub/issues/6))

* :heavy_check_mark: [**NodeInfo2**](https://git.feneas.org/jaywink/nodeinfo2): An effort to create a standardized way of exposing metadata about a server. Helps expose ownership and organization details, usage statistics and protocol capabilities. `CC0-1.0`

* :black_nib: [**Podcasting**](https://github.com/Podcastindex-org/activitypub-spec-work): Workspace for defining ActivityPub and ActivityStream extensions for Podcasting with the intention to define a W3C Recommendation for them.

* :heavy_check_mark: [**SciFed**](https://synalp.frama.io/olki/scifed/) (Fedi accounts: [@rigelk](https://olki-social.loria.fr/@rigelk), [@cerisara](https://mastodon.etalab.gouv.fr/@cerisara)): A specification standard (Draft) for federation of scientific activities and content using ActivityPub, developed as part of the [OLKi](https://olki.loria.fr/) project. 

#### API documentation

## Tutorials

#### Getting started

#### ActivityPub server-to-server (S2S)

#### ActivityPub client-to-server (C2S)

#### WebFinger

#### Security

#### NodeInfo

## Research & Development

#### Datashards

#### Object capabilities

#### Federated auth/authz

* :black_nib: [**The did:orb Method**](https://trustbloc.github.io/did-method-orb/) ([github](https://github.com/trustbloc/did-method-orb)): A DID Method for a fediverse of interconnected nodes and witnesses.

#### Content addressing

* :black_nib: [**Content-addressible RDF**](https://openengiadina.net/papers/content-addressable-rdf.html): A scheme based on RDF allowing for data to be referred to by an identifier determined by the data itself (written by @pukkamustard of [openEngiadina](https://openengiadina.net), specification now part of [DREAM](https://dream.public.cat/pub/dream-data-spec))

* :black_nib: [**Encoding for Robust Immutable Storage** (ERIS)](https://inqlab.net/projects/eris/): ERIS is an encoding of arbitrary content into a set of uniformly sized, encrypted and content-addressed blocks as well as a short identifier (a URN) (written by @pukkamustard of [openEngiadina](https://openengiadina.net), specification now part of [DREAM](https://dream.public.cat/pub/dream-data-spec)).

#### Distributed Mutable Containers

* :black_nib: [**Distributed Mutable Containers** (DMC)](https://inqlab.net/projects/dmc/): Distributed data structures that can hold references to content while allowing replicas of the data structures to diverge and merge without conflict (developed by [DREAM](https://dream.public.cat/pub/dream-data-spec))

#### Peer-to-peer networking

## Maintainers

If you have questions or feedback regarding this list, then please create an [Issue](https://codeberg.org/fediverse/delightful-activitypub-development/issues) in our tracker, and optionally `@mention` one or more of our maintainers:

- [`@circlebuilder`](https://codeberg.org/circlebuilder)
- [`@lostinlight`](https://codeberg.org/lostinlight)

## Contributors

With delight we present you some of our [delightful contributors](delightful-contributors.md) (please [add yourself](https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-us.md#attribution-of-contributors) if you are missing).

## License

[![CC0 Public domain. This work is free of known copyright restrictions.](https://i.creativecommons.org/p/mark/1.0/88x31.png)](LICENSE)